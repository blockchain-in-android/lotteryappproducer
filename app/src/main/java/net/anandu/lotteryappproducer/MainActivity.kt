package net.anandu.lotteryappproducer

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.HourglassEmpty
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dev.burnoo.compose.rememberpreference.rememberStringPreference
import kotlinx.coroutines.launch
import net.anandu.blocklib.BlockLib
import net.anandu.blocklib.blockchain.Encryption
import net.anandu.blocklib.database.Transaction
import net.anandu.blocklib.util.PoAGenesisConfig
import net.anandu.lotteryappproducer.ui.screens.create_lottery.CreateLottery
import net.anandu.lotteryappproducer.ui.screens.create_lottery.TicketList
import net.anandu.lotteryappproducer.ui.screens.lottery_list.LotteryList
import net.anandu.lotteryappproducer.ui.screens.lottery_page.LotteryPage
import net.anandu.lotteryappproducer.ui.screens.verify_ticket.VerifyTicketPage
import net.anandu.lotteryappproducer.ui.theme.LotteryAppProducerTheme
import java.math.BigInteger
import java.security.MessageDigest
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class MainActivity : ComponentActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    @ExperimentalMaterialApi
    @ExperimentalComposeUiApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LotteryAppProducerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MainPage()
                }
            }
        }
    }
}

suspend fun BlockLib.queryTransactions(query: String): List<Transaction> =
    suspendCoroutine { cont ->
        val callback = { l: List<Transaction> -> cont.resume(l) }
        this.searchInTransactionData(query, callback)
    }

fun getRandomString(length: Int): String {
    val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
    return (1..length)
        .map { allowedChars.random() }
        .joinToString("")
}

fun md5(input: String): String {
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}

fun BlockLib.createLottery(
    id: String,
    name: String,
    startDate: String,
    endDate: String,
    maxTickets: Int
): Array<String> {
    val tickets = Array(5) { _ -> getRandomString(5) }
    this.createTransaction(
        "SELF", "ALL", 0, mapOf(
            "id" to id,
            "type" to "LOTTERY_START",
            "name" to name,
            "startDate" to startDate,
            "endDate" to endDate,
            "maxTickets" to maxTickets.toString(),
            "ticketHashes" to tickets.map { md5(it) }.toString(),
        )
    )
    return tickets
}

suspend fun BlockLib.drawLottery(lotteryId: String) {
    // todo: validate
    val ticketTransactions =
        this.queryTransactions(lotteryId).filter { it.data["type"] == "CLAIM_TICKET" }
    val winner = ticketTransactions.asSequence().shuffled().find { true }
        ?: throw Exception("No one purchased tickets?")

    this.createTransaction(
        "SELF", "ALL", 0, mapOf(
            "lotteryId" to lotteryId,
            "type" to "DRAW_LOTTERY",
            "winnerHash" to winner.data["secret"]
        )
    )
}


suspend fun BlockLib.checkWinner(lotteryId: String, ticketId: String, secret: String): Boolean {
    // find the transaction of the lottery
    val winner = this.queryTransactions(lotteryId).firstOrNull { it.data["type"] == "DRAW_LOTTERY" }
        ?: throw Exception("Winner has not been declared yet")

    val computedSecret = md5(ticketId + secret)
    println(winner)
    println("lotteryId= $lotteryId, ticketId= $ticketId secret=$secret computed: $computedSecret")
    if (winner.data["winnerHash"] == computedSecret) {
        return true;
    }
    return false;
}


@RequiresApi(Build.VERSION_CODES.O)
@ExperimentalMaterialApi
@ExperimentalComposeUiApi
@Composable
fun MainPage() {
    val coroutineScope = rememberCoroutineScope();
    val navController = rememberNavController()
    val encryption = (object : Encryption() {});
    var publicKey by rememberStringPreference(
        keyName = "publicKey", // preference is stored using this key
        initialValue = null, // returned before preference is loaded
        defaultValue = "", // returned when preference is not set yet
    )
    var privateKey by rememberStringPreference(
        keyName = "privatekey", // preference is stored using this key
        initialValue = null, // returned before preference is loaded
        defaultValue = "", // returned when preference is not set yet
    )

    val savedLotteries = remember {
        mutableStateListOf<Transaction>()
    }

    if (privateKey == "" && publicKey == "") {
        println("Generating new keyPair")
        privateKey = encryption.privateKeyString;
        publicKey = encryption.pubKeyString;
    }
    if (privateKey == null || publicKey == null || publicKey == "" || privateKey == "") {
        Column(
            Modifier
                .padding(8.dp)
                .fillMaxWidth()
                .fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Icon(Icons.Outlined.HourglassEmpty, null)
            Text("Loading...")
        }
        return;
    }
    println("privateKey: $privateKey")
    println("publicKey: $publicKey")

    val kp = Encryption.getKeyPairFromEncoded(publicKey, privateKey)

    val blockLib = object : BlockLib() {
        override fun onConnected() {
            println("Not yet implemented")
        }

        override fun onTransactionAdded(t: Transaction?) {
            println(t)
            println("Not yet implemented")
        }

        override fun onSyncComplete() {
            println("Not yet implemented")
        }

        override fun onBlockAdded(blockNum: Int?) {
            println("Not yet implemented")
        }

    }
    val peerId = "example3";
    println("AuthorityNodes: " + listOf(Encryption.getAddress(kp)))
    val genConfig = PoAGenesisConfig(listOf(Encryption.getAddress(kp)), "", 5);
    blockLib.init(LocalContext.current, peerId, kp, genConfig, fun(t: Transaction): Boolean {
        if (t.timestamp == "0") {
            return false;
        }
        return true;
    })

    fun loadLotteries() {
        coroutineScope.launch {
            val t = blockLib.queryTransactions("LOTTERY_START")
            savedLotteries.clear()
            savedLotteries.addAll(t)
        }
    }

    LaunchedEffect("startup") {
        loadLotteries()
        val exec = Executors.newSingleThreadScheduledExecutor()
        exec.scheduleAtFixedRate({
            try {
                println("Creating block now!!!")
                blockLib.createBlock(
                    mapOf(
                        "example" to "A"
                    )
                )
            } catch (e: Exception) {
                System.err.println(e)
            }
        }, 0, 30, TimeUnit.SECONDS)
    }


    NavHost(navController = navController, startDestination = "lottery_list") {
        composable("lottery_list") {
            LotteryList(
                navController,
                messages = savedLotteries,
                blockLib,
            ) { loadLotteries() }
        }
        composable("ticket_list/{tickets}") { backStackEntry ->
            val ticketString = backStackEntry.arguments?.getString("tickets") ?: "[]"
            val tickets = ticketString.drop(1).dropLast(1).split(",")
            TicketList(navController, tickets, blockLib)
        }
        composable("create_lottery") { CreateLottery(navController, blockLib) }
        composable("lottery_page/{lotteryId}") { backStackEntry ->
            LotteryPage(
                navController,
                backStackEntry.arguments?.getString("lotteryId") ?: "ERROR",
                blockLib
            )
        }
        composable("verify_ticket/{lotteryId}") { backStackEntry ->
            val lotteryId = backStackEntry.arguments?.getString("lotteryId") ?: "ERROR"
            VerifyTicketPage(
                navController,
                lotteryId,
                blockLib
            )
        }
    }
}

@ExperimentalMaterialApi
@ExperimentalComposeUiApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LotteryAppProducerTheme {
        MainPage()
    }
}
