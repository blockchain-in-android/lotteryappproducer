package net.anandu.lotteryappproducer.ui.screens.lottery_page

import android.widget.Toast
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Money
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import net.anandu.blocklib.BlockLib
import net.anandu.blocklib.database.Transaction
import net.anandu.lotteryappproducer.drawLottery
import net.anandu.lotteryappproducer.queryTransactions

// Show the tickets + ticketStatus
// CheckWinnerButton
// Declare Draw

@Composable
@ExperimentalMaterialApi
fun LotteryPage(
    navController: NavHostController,
    lotteryId: String,
    blockLib: BlockLib,
) {

    var offset by remember { mutableStateOf(0f) }
    val ticketTransactions = remember { mutableStateListOf<Transaction>() }
    val coroutineScope = rememberCoroutineScope()
    var lotteryStatus by remember { mutableStateOf("Loading") }
    val context = LocalContext.current
    // tickets


    fun loadData() {
        coroutineScope.launch {
            val t = blockLib.queryTransactions(lotteryId)

            var status = "ACTIVE";
            val tickets = mutableListOf<Transaction>()

            t.forEach {
                when {
                    it.data["type"] == "CREATE_LOTTERY" -> {
                        println("todo: extract startDate, endDate etc info")
                    }
                    it.data["type"] == "CLAIM_TICKET" -> {
                        tickets.add(it)
                    }
                    it.data["type"] == "DRAW_LOTTERY" -> {
                        status = "COMPLETED"
                    }
                }
            }

            ticketTransactions.clear()
            ticketTransactions.addAll(tickets)
            lotteryStatus = status;
        }
    }
    // get the lottery start transaction data
    LaunchedEffect("fetch_claims") {
        loadData()
    }

    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxSize()
    ) {
        TopAppBar(
            elevation = 4.dp,
            title = {
                Text("Lottery Producer")
            },
            backgroundColor = MaterialTheme.colors.primarySurface,
            actions = {
                IconButton(onClick = {
                    navController.navigate("lottery_list")
                }) {
                    Icon(Icons.Filled.Home, null)
                }
                IconButton(onClick = {
                    loadData()
                }) {
                    Icon(Icons.Filled.Refresh, null)
                }
                IconButton(onClick = {
                    navController.navigate("create_lottery")
                }) {
                    Icon(Icons.Filled.Add, null)
                }
            })

        Column(
            Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {

            LotteryInfo(name = lotteryId, state = lotteryStatus)
            Spacer(Modifier.height(16.dp))
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                if (lotteryStatus == "ACTIVE") {
                    Button(onClick = {
                        coroutineScope.launch {
                            try {
                                lotteryStatus = "COMPLETED"
                                blockLib.drawLottery(lotteryId)
                                Toast.makeText(context, "Winner declared", Toast.LENGTH_LONG)
                                    .show();
                            } catch (e: Exception) {
                                Toast.makeText(context, "Error: " + e.message, Toast.LENGTH_LONG)
                                    .show();
                            }
                        }
                    }) {
                        Text("Declare Draw")
                    }
                } else {
                    Button(onClick = {
                        navController.navigate("verify_ticket/$lotteryId")
                    }) {
                        Text("Verify Ticket")
                    }
                }
            }
            Spacer(Modifier.height(16.dp))
            if (ticketTransactions.isEmpty()) {
                Text("Tickets are yet to be claimed")
            }

            LazyColumn(modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight()
                .scrollable(
                    orientation = Orientation.Vertical,
                    state = rememberScrollableState { delta ->
                        offset += delta
                        delta
                    }
                )) {
                items(ticketTransactions) { transaction ->
                    Box(modifier = Modifier.padding(8.dp)) {
                        TicketListItem(
                            ticketId = transaction.data["ticketId"].toString(),
                            ticketStatus = "CLAIMED",
                            purchaseTime = transaction.timestamp,
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun LotteryInfo(name: String, state: String) {
    Card(
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .height(180.dp)
    ) {
        Column(
            Modifier
                .padding(8.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Text(name, style = MaterialTheme.typography.h4)
            Button(
                onClick = {},
                colors = ButtonDefaults.buttonColors(backgroundColor = Color.Green),
                modifier = Modifier.fillMaxWidth().padding(horizontal = 8.dp)
            ) {
                Text(state, color = Color.White)
            }
        }
        Spacer(Modifier.height(4.dp))
    }

}


@ExperimentalMaterialApi
@Composable
fun TicketListItem(ticketId: String, ticketStatus: String, purchaseTime: String) {
    Card(
        elevation = 8.dp,
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
    ) {
        Row(Modifier.padding(8.dp), verticalAlignment = Alignment.CenterVertically) {
            Icon(
                Icons.Filled.Money, "money",
                Modifier
                    .fillMaxHeight()
                    .padding(4.dp, end = 8.dp)
            )
            Column() {
                Text(
                    buildAnnotatedString {
                        withStyle(
                            style = SpanStyle(
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        ) {
                            append(ticketId)
                        }
                        append(" - ")
                        withStyle(style = SpanStyle(color = Color.Gray)) {
                            append(ticketStatus)
                        }
                    }
                )
                Text(text = purchaseTime, fontSize = 12.sp, color = Color.Gray)
            }
        }
    }

}