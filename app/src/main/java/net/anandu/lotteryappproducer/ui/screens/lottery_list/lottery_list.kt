package net.anandu.lotteryappproducer.ui.screens.lottery_list

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Redeem
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import net.anandu.blocklib.BlockLib
import net.anandu.blocklib.database.Transaction

// List of the lotteries on the chain

@ExperimentalMaterialApi
@Composable
fun LotteryListItem(
    transaction: Transaction,
    navController: NavController,
) {
    println(transaction)
    val lotteryId = transaction.data["id"]
    Card(
        elevation = 8.dp,
        onClick = {
            navController.navigate("lottery_page/$lotteryId")
        },
        modifier = Modifier
            .fillMaxWidth()
            .height(64.dp)
    ) {
        Row(Modifier.padding(8.dp)) {
            Icon(Icons.Filled.Redeem, "menu", Modifier.fillMaxHeight().padding(4.dp))
            Column(Modifier.padding(start = 8.dp)) {
                Text(
                    buildAnnotatedString {
                        withStyle(
                            style = SpanStyle(
                                color = Color.Gray,
                            )
                        ) {
                            append("Id : ")
                        }
                        withStyle(
                            style = SpanStyle(
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        ) {
                            append(transaction.data["id"].toString())
                        }
                    }
                )
                Text(
                    buildAnnotatedString {
                        withStyle(
                            style = SpanStyle(
                                color = Color.Gray,
                            )
                        ) {
                            append("Name : ")
                        }
                        withStyle(
                            style = SpanStyle(
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        ) {
                            append(transaction.data["name"].toString())
                        }
                    }
                )
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun LotteryList(
    navController: NavController,
    messages: List<Transaction>,
    blockLib: BlockLib,
    loadLotteries: () -> Unit,
) {
    var offset by remember { mutableStateOf(0f) }
    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxSize()
    ) {
        TopAppBar(
            elevation = 4.dp,
            title = {
                Text("Lottery Producer")
            },
            backgroundColor = MaterialTheme.colors.primarySurface,
            actions = {
                IconButton(onClick = {
                    loadLotteries()
                }) {
                    Icon(Icons.Filled.Refresh, null)
                }
                IconButton(onClick = {
                    navController.navigate("create_lottery")
                }) {
                    Icon(Icons.Filled.Add, null)
                }
            })
        LazyColumn(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .padding(vertical = 8.dp)
            .scrollable(
                orientation = Orientation.Vertical,
                state = rememberScrollableState { delta ->
                    offset += delta
                    delta
                }
            )) {
            items(messages) { transaction ->
                Box(modifier = Modifier.padding(8.dp).fillMaxHeight()) {
                    LotteryListItem(transaction, navController)
                }
            }
        }

    }
}

