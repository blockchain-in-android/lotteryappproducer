package net.anandu.lotteryappproducer.ui.screens.verify_ticket

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import kotlinx.coroutines.launch
import net.anandu.blocklib.BlockLib
import net.anandu.lotteryappproducer.checkWinner
import net.anandu.lotteryappproducer.ui.screens.lottery_page.LotteryInfo

// Take LOTTERY_INF + SECRET => VERIFY
// display lottery info + ticket Info
// input for secret
// alert valid or not

@Composable
fun VerifyTicketPage(navHostController: NavHostController,lotteryId: String, blockLib: BlockLib) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        var secret by remember { mutableStateOf("") }
        var ticketId by remember { mutableStateOf("") }
        val context = LocalContext.current
        val coroutineScope = rememberCoroutineScope();
        Text(
            text = "Check Winner",
            style = MaterialTheme.typography.h4
        )
        Spacer(Modifier.size(16.dp))
        LotteryInfo(name = lotteryId, state = "DRAW Completed")

        Spacer(Modifier.size(8.dp))

        TextField(
            value = ticketId,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                ticketId = newText
            },
            label = {
                Text(text = "Ticket Id")
            }
        )
        TextField(
            value = secret,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                secret = newText
            },
            label = {
                Text(text = "Secret")
            }
        )
        Spacer(Modifier.size(20.dp))
        Button(onClick = {
            coroutineScope.launch {
                try {
                    val winner = blockLib.checkWinner(lotteryId = lotteryId, ticketId=ticketId, secret =secret  )
                    if ( winner ) {
                        Toast.makeText(context, "You Are the Winner", Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(context, ":( Invalid Ticket", Toast.LENGTH_LONG).show()
                    }
                } catch ( e: Exception ) {
                    Toast.makeText(context, "Error: " + e.message, Toast.LENGTH_LONG).show()
                }
            }
        }) {
            Text(text = "Check")
        }
    }
}