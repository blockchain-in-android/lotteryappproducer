package net.anandu.lotteryappproducer.ui.screens.create_lottery

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import net.anandu.blocklib.BlockLib
import net.anandu.lotteryappproducer.createLottery

// take data and create a lottery
@Composable
fun CreateLottery(navHostController: NavHostController, blockLib: BlockLib) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        verticalArrangement = Arrangement.Center
    ) {
        var name by remember { mutableStateOf("") }
        var startDate by remember { mutableStateOf("") }
        var endDate by remember { mutableStateOf("") }
        var maxTickets by remember { mutableStateOf(5) }


        Text(
            text = "Create Lottery",
            style = MaterialTheme.typography.h4
        )
        Spacer(Modifier.size(16.dp))

        TextField(
            value = name,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                name = newText
            },
            label = {
                Text(text = "Name")
            }
        )
        TextField(
            value = startDate,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                startDate = newText
            },
            label = {
                Text(text = "Start Date")
            }
        )
        TextField(
            value = endDate,
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            onValueChange = { newText ->
                endDate = newText
            },
            label = {
                Text(text = "End date")
            }
        )
        TextField(
            value = maxTickets.toString(),
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 8.dp),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
            onValueChange = { newText: String ->
                maxTickets = try {
                    newText.toInt()
                } catch (e: Exception) {
                    5;
                }
            },
            label = {
                Text(text = "Max tickets")
            }
        )

        Spacer(Modifier.size(16.dp))
        Button(onClick = {
            val tickets: Array<String> = blockLib.createLottery(
                id = name,
                name = name,
                endDate = "a",
                startDate = "b'",
                maxTickets = 5,
            )

            println("Tickets: ${tickets.contentToString()}")


            navHostController.navigate("ticket_list/${tickets.contentToString()}")

        }) {
            Text(text = "Create")
        }
    }
}

