package net.anandu.lotteryappproducer.ui.screens.create_lottery

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.common.BitMatrix
import net.anandu.blocklib.BlockLib


@Composable
fun TicketList(
    navController: NavHostController,
    tickets: List<String>,
    blockLib: BlockLib
) {
    var offset by remember { mutableStateOf(0f) }
    Column(
        Modifier
            .fillMaxWidth()
            .fillMaxSize()
    ) {
        TopAppBar(
            elevation = 4.dp,
            title = {
                Text("Lottery Producer")
            },
            backgroundColor = MaterialTheme.colors.primarySurface,
            actions = {
                IconButton(onClick = {
                    navController.navigate("lottery_list")
                }) {
                    Icon(Icons.Filled.Home, null)
                }
            })

        LazyColumn(modifier = Modifier
            .fillMaxWidth()
            .scrollable(
                orientation = Orientation.Vertical,
                state = rememberScrollableState { delta ->
                    offset += delta
                    delta
                }
            )) {
            items(tickets) { ticket ->
                Card(modifier = Modifier.padding(8.dp), elevation = 8.dp) {
                    Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
                        Image(
                            bitmap = buildQrCode(ticket)!!.asImageBitmap(),
                            contentDescription = "some useful description",
                        )
                        Column(
                            Modifier.height(IntrinsicSize.Max),
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            Text("Ticket Number", style = MaterialTheme.typography.h6)
                            Text(ticket, style = MaterialTheme.typography.h4)
                        }
                    }
                }
            }
        }
    }

}

var white = -0x1
var black = -0x1000000

fun buildQrCode(str: String): Bitmap? {
    val bitMatrix: BitMatrix = try {
        MultiFormatWriter().encode(
            "HI there",
            BarcodeFormat.QR_CODE,
            500, 500, null
        )
    } catch (iae: IllegalArgumentException) {
        return null
    }
    val bitMatrixWidth = bitMatrix.width
    val bitMatrixHeight = bitMatrix.height
    val pixels = IntArray(bitMatrixWidth * bitMatrixHeight)
    for (y in 0 until bitMatrixHeight) {
        val offset = y * bitMatrixWidth
        for (x in 0 until bitMatrixWidth) {
            pixels[offset + x] =
                if (bitMatrix[x, y]) black else white
        }
    }
    val bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444)
    bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight)
    return bitmap
}